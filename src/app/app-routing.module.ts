import { NgModule } from '@angular/core';
import {Routes, RouterModule } from '@angular/router';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { EnterpriseUserComponent } from './components/enterprise-user/enterprise-user.component';




// const routes:  Routes =[

//     { path: 'user', component: UserComponent},
//     { path: 'login', component: LoginComponent},
//     { path: 'register-user', component: RegisterUserComponent },
//     { path: 'enterprise-user', component: EnterpriseUserComponent },
//     { path: '**', redirectTo: 'user', pathMatch: 'full' } 

// ]

// @NgModule({
//     imports: [RouterModule.forRoot(routes)],
//     exports: [RouterModule]
// })

// export class AppRoutingModule{ } 