import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AdminUserComponent } from './components/admin-user/admin-user.component';
import { UserComponent } from './components/user/user.component';
import { EnterpriseUserComponent } from './components/enterprise-user/enterprise-user.component';
//import { AppRoutingModule } from './app-routing.module';
import { CandidateUserComponent } from './components/candidate-user/candidate-user.component';
import { RegisterEnterpriseComponent } from './components/register-enterprise/register-enterprise.component';
import { OfferRegistrationComponent } from './components/offer-registration/offer-registration.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { EditEnterpriseComponent } from './components/edit-enterprise/edit-enterprise.component';

import {MatToolbarModule,} from '@angular/material/toolbar';
import {MatButtonModule, MatFormFieldModule, MatIconModule, MatListModule} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import { SidenavComponent } from './components/shared/sidenav/sidenav.component';
import {MatTabsModule} from '@angular/material/tabs';



import { APP_ROUTES } from '../app/app.routes';

import {MatCardModule} from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatStepperModule} from '@angular/material/stepper';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import { environment } from "src/environments/environment";
import { AngularFireModule } from "@angular/fire";
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { SidebarEnterpriseComponent } from './components/shared/sidebar-enterprise/sidebar-enterprise.component';
import { EnterpriseProfileComponent } from './components/enterprise-profile/enterprise-profile.component';
import { CandidateCVComponent } from './components/candidate-cv/candidate-cv.component';
import { StudyFormComponent } from './components/cv_forms/study-form/study-form.component';
import { WorkExperienceComponent } from './components/cv_forms/work-experience/work-experience.component';
import { SidebarCandidateComponent } from './components/shared/sidebar-candidate/sidebar-candidate.component';
import { OtherCoursesComponent } from './components/cv_forms/other-courses/other-courses.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminUserComponent,
    UserComponent,
    EnterpriseUserComponent,
    CandidateUserComponent,
    NavbarComponent,
    SidenavComponent,
    RegisterUserComponent,
    RegisterEnterpriseComponent,
    OfferRegistrationComponent,
    EditEnterpriseComponent,
    SidebarEnterpriseComponent,
    EnterpriseProfileComponent,
    CandidateCVComponent,
    StudyFormComponent,
    WorkExperienceComponent,
    SidebarCandidateComponent,
    OtherCoursesComponent,
  ],
  imports: [
    BrowserModule,
    //AppRoutingModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule ,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatTabsModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatIconModule,
    MatPaginatorModule,
    MatMenuModule,
    MatTableModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatRadioModule,
    MatDatepickerModule
   ,MatNativeDateModule,MatInputModule,
    MatStepperModule,
    FormsModule,
    MatSnackBarModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireStorageModule,
    APP_ROUTES
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
