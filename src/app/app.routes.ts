import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { CandidateUserComponent } from './components/candidate-user/candidate-user.component';
import { EnterpriseUserComponent } from './components/enterprise-user/enterprise-user.component';
import { RegisterEnterpriseComponent } from './components/register-enterprise/register-enterprise.component';
import { OfferRegistrationComponent } from './components/offer-registration/offer-registration.component';
import { AdminUserComponent } from './components/admin-user/admin-user.component';
import { EditEnterpriseComponent } from './components/edit-enterprise/edit-enterprise.component';
import { EnterpriseProfileComponent } from './components/enterprise-profile/enterprise-profile.component';
import { CandidateCVComponent } from './components/candidate-cv/candidate-cv.component';
import { StudyFormComponent } from './components/cv_forms/study-form/study-form.component';
import { WorkExperienceComponent } from './components/cv_forms/work-experience/work-experience.component';
import { OtherCoursesComponent } from './components/cv_forms/other-courses/other-courses.component';


const ROUTES: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'register-user', component: RegisterUserComponent},
    {path: 'user-profile', component: CandidateUserComponent},
    {path: 'enterprise-profile', component: EnterpriseUserComponent},
    {path: 'register-enterprise', component: RegisterEnterpriseComponent},
    {path: 'edit-enterprise/:id', component: EditEnterpriseComponent},
    {path: 'admin-profile', component: AdminUserComponent},
    {path: 'offer-registration', component: OfferRegistrationComponent},
    {path: 'enterprise-profile-info', component: EnterpriseProfileComponent},
    {path: 'candidate-cv', component: CandidateCVComponent},
    {path: 'cv-study-form', component: StudyFormComponent},
    {path: 'cv-work-experience', component: WorkExperienceComponent},
    {path: 'cv-other-courses', component: OtherCoursesComponent},
    {path:'',pathMatch:'full',redirectTo:'login'},
    {path:'**',pathMatch:'full',redirectTo:'login'}
];

export const APP_ROUTES = RouterModule.forRoot(ROUTES);