import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { EnterpriseService } from '../../services/enterprise-service.service';
import { EnterpriseModel } from '../../models/enterprise.model';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

const ELEMENT_DATA: EnterpriseModel[] = [];
@Component({
  selector: 'app-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.css']
})


export class AdminUserComponent implements OnInit {

  displayedColumns: string[] = ['position','razonSocial', 'NIT', 'actividadPrincipal', 'aprobacion', 'options'];
  enterpriseList: EnterpriseModel[] = []
  dataSource = ELEMENT_DATA;
  dataSourceService = new MatTableDataSource<EnterpriseModel>();


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    //this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor(private enterpriseService:EnterpriseService,
            private changeDetectorRefs: ChangeDetectorRef,
            private route: Router,
            private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getAllEnterprise();
    
  }



  editEnterpise(uid){ 
    this.route.navigateByUrl(`/edit-enterprise/${uid}`);
    console.log(`/edit-enterprise'${uid}`);
    console.log(uid);
  }


  

  getAllEnterprise(){
    this.dataSource=[];
    this.dataSourceService.data = [];
    this.enterpriseService.getEnterpriseList().subscribe((resp)=>{
      this.dataSource=[];
      this.dataSourceService.data = [];
      
      resp.forEach((enterprise: any) => {
        this.dataSource.push(this.enterpriseService.parseEnterpriseModel(enterprise.payload.doc.data()));
        this.dataSourceService.data = this.dataSource;
      });
      console.log(this.dataSource);
    })
  }


  deleteEnterprise(enterpriseValue){
    this.enterpriseService.deleteEnterprice(enterpriseValue);
  }

}                           
