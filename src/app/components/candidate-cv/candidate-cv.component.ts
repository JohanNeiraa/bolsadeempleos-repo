import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, FormArray} from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import { User } from 'firebase';
import { CurriculumModel } from '../../models/curriculum.model';
import { Observable } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/storage';
import { UserService } from '../../services/user.service';
import { finalize } from 'rxjs/operators';

@Component({
  templateUrl: './candidate-cv.component.html',
  styleUrls: ['./candidate-cv.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]
})
export class CandidateCVComponent implements OnInit {
  myControl = new FormControl();
  curriculum:CurriculumModel = null;
  private image: any;
  options: string[] = [ 'SMLV',
                        '1.000.000 a 2.000.000', 
                        '2.000.000 a 3.000.000', 
                        '3.000.000 a 5.000.000',
                        '5.000.000 a 8.000.000'];

  curriculumId;

  hideRequiredControl = new FormControl(false);
  form: FormGroup;
  public idCandidate;
  constructor(private _formBuilder: FormBuilder, private storage: AngularFireStorage,private userService:UserService) { }

  ngOnInit() {
    //por ahora
    this.downloadUrl=localStorage.getItem('url');
    this.createForm();
    this.getUserCurriculum();
  }

  validateLicense(e){
    if(e.checked){
      this.form.get("licencia").enable();
      this.form.get("licencia").setValidators([Validators.required]);
    }else{
      this.form.get("licencia").disable();

    } 
    this.form.get("licencia").updateValueAndValidity();
  }

  send(){
    if(this.form.valid){
      if(this.downloadUrl){
        localStorage.setItem('url',this.downloadUrl)
      }
    }
    
    console.log(this.form.value)  
    console.log(this.curriculumId);
    if(this.form.valid){
      let uidUser = localStorage.getItem('useruid');
      this.userService.updateCurriculum(this.form.value,this.curriculumId,uidUser,this.curriculum).then((data)=>{
        console.log(data);
      },(error)=>{
        console.log(error);
      });
    }
    
    //console.log(this.form.value);
  }

  get language(){
    
    return this.form.get('idiomas') as FormArray
  }

  get experiencias(){
    
    return this.form.get('experienciaLaboral') as FormArray
  }

  get otrosEstudios(){
    
    return this.form.get('otrosEstudios') as FormArray
  }

    
  deleteLanguage(i){
    this.language.removeAt(i);
  }

  addLenguage(){
    
    const idioma = this._formBuilder.group({
      idiomaaaa: ['',Validators.required],
      nivelIdioma: ['',Validators.required],
    });
    this.language.push(idioma);
  }

  addOtherStudies(){
    const otrosEstudios =this._formBuilder.group({
      tipoEstudio: ['', Validators.required],
      tituloOtorgado: ['', Validators.required],        
      institucion: ['', Validators.required],
      fechaInicio: ['', Validators.required],
      fechaFinalizacion: ['', Validators.required],
      ciudad: ['', Validators.required],
      descripcion: ['', Validators.required],
    } );
    this.otrosEstudios.push(otrosEstudios);
  }

  addLaboralExperience(){

    const experiencia = this._formBuilder.group({
      nombreEmpresa: ['', Validators.required],
      fechaInicio: ['', Validators.required],
      fechaFinalizacion: ['', Validators.required],
      nombreCargo: ['', Validators.required],
      nivelCargo: ['', Validators.required],
      areaDepartamento: ['', Validators.required],
      logrosRespon: ['', Validators.required],
      telefonoEmpre: ['', Validators.required],
      ciudadEmpre: ['', Validators.required],
    } );
    this.experiencias.push(experiencia);
  }

  addComplementaryStudy(){
    this.userService.updateComplementaryStudies(this.otrosEstudios.value[0],this.curriculum,this.curriculumId)
  }


  deleteIdiom(index){
    console.log(index);
    this.userService.deleteLanguage(this.curriculum,this.curriculumId,index);
  }

  deleteExp(index){
    this.userService.deleteExperience(this.curriculum,this.curriculumId,index);
  }
  

  deleteComplementary(index){
    this.userService.deleteComplementary(this.curriculum,this.curriculumId,index);
  }

  validateStatus(e){
    if(e.checked){
      this.form.get("fechaFinalizacion").enable();
      this.form.get("fechaFinalizacion").setValidators([Validators.required]);
    }else{
      this.form.get("fechaFinalizacion").disable();
    }
    this.form.get("fechaFinalizacion").updateValueAndValidity();
  }


  handleImage(e:any){
    this.image = e.target.files[0];
    console.log(this.image);

    this.uploadImage(this.image);
  }

  private filePath:any;
  downloadUrl: any;


  private uploadImage(image){
    this.filePath = `image/${image.name}`;
    const fileRef = this.storage.ref(this.filePath);
    const task = this.storage.upload(this.filePath, image);

    task.snapshotChanges().pipe(
      finalize(()=>{
        fileRef.getDownloadURL().subscribe(urlImagen=>{
          this.downloadUrl=urlImagen;
          console.log("url", urlImagen);
          console.log("url", urlImagen)
        })
      }
      )
    ).subscribe();
  }

  addExperience(){
    console.log(this.experiencias);
    console.log('agregando experiencia');
    this.userService.updateLaboralExperience(this.experiencias.value[0],this.curriculum,this.curriculumId).then((data:any)=>{
      console.log(data);
    },(error)=>{
      console.log(error);
    });
  }

  saveLanguage(){
    console.log(this.language);
    this.userService.updateLanguages(this.language.value[0],this.curriculum,this.curriculumId);
  }

  createForm() {
    if(this.curriculum != null){
      this.form = this._formBuilder.group({
        idCandidate:[this.idCandidate],
        imagen:[''],
        nombres: [this.curriculum.names, Validators.required],        
        apellido1: [this.curriculum.lastName, Validators.required],
        apellido2: [this.curriculum.secondLastName, Validators.required],
        tipoIdentificacion: [this.curriculum.identificationType, Validators.required],
        numeroIdentificación: [this.curriculum.identification, Validators.required],
        fechaNacimiento: [this.curriculum.bornDate, Validators.required],
        genero: [this.curriculum.genre, Validators.required],
        estadoCivil: [this.curriculum.civilState, Validators.required],
        CiudadResidencia: [this.curriculum.liveCity, Validators.required],
        DireccionResidencia: [this.curriculum.address, Validators.required],
        ciudadNacimiento: [this.curriculum.bornCity, Validators.required],
        telefonoPrincipal: [this.curriculum.principalPhone, Validators.required],
        telefonoSecundario: [this.curriculum.secondaryPhone, Validators.required],
        correoElectronico: [this.curriculum.email, Validators.required],
        profesion: [this.curriculum.profession, Validators.required],
        aspiracionSalarial: [this.curriculum.salary, Validators.required],
        movilidadLaboral: ['', Validators.required],
        perfilLaboral: [this.curriculum.laboralProfile, Validators.required],
        experienciaLaboral: this._formBuilder.array([]),
        formacionAcademica: this._formBuilder.group({
          nivelEstudios: [this.curriculum.academicFormation.studyLevel],
          estado: [this.curriculum.academicFormation.studyState],
          tituloOtorgado: [this.curriculum.academicFormation.studyTitle],
          fechaInicio: [this.curriculum.academicFormation.initDate],
          fechaFinalizacion: [this.curriculum.academicFormation.finalDate],
          nombreInstitucion: [this.curriculum.academicFormation.schoolName],
        }),
        otrosEstudios: this._formBuilder.array([]),
        carreraPrincipal: [this.curriculum.principalCarrer, Validators.required],
        lenguaNativa: [this.curriculum.nativeLanguage, Validators.required],
        idiomas: this._formBuilder.array([]),
        destrezas: [this.curriculum.habilities, Validators.required],
        licencia: [this.curriculum.driverLicenceType, Validators.required],
      } );
    }else{

      this.form = this._formBuilder.group({
        idCandidate:[this.idCandidate],
        nombres: ['', Validators.required],        
        apellido1: ['', Validators.required],
        apellido2: ['', Validators.required],
        imagen:[''],
        tipoIdentificacion: ['', Validators.required],
        numeroIdentificación: ['', Validators.required],
        fechaNacimiento: ['', Validators.required],
        genero: ['', Validators.required],
        estadoCivil: ['', Validators.required],
        CiudadResidencia: ['', Validators.required],
        DireccionResidencia: ['', Validators.required],
        ciudadNacimiento: ['', Validators.required],
        telefonoPrincipal: ['', Validators.required],
        telefonoSecundario: ['', Validators.required],
        correoElectronico: ['', Validators.required],
        profesion: ['', Validators.required],
        aspiracionSalarial: ['', Validators.required],
        movilidadLaboral: ['', Validators.required],
        perfilLaboral: ['', Validators.required],
        experienciaLaboral: this._formBuilder.array([]),
        formacionAcademica: this._formBuilder.group({
          nivelEstudios: [''],
          estado: [''],
          tituloOtorgado: [''],
          fechaInicio: [''],
          fechaFinalizacion: [''],
          nombreInstitucion: [''],
        }),
        otrosEstudios: this._formBuilder.array([]),
        carreraPrincipal: ['', Validators.required],
        lenguaNativa: ['', Validators.required],
        idiomas: this._formBuilder.array([]),
        destrezas: ['', Validators.required],
        licencia: ['', Validators.required],
      } );
    }
  }

  getUserCurriculum(){
    let uidUser = localStorage.getItem('useruid');
    this.userService.getUserCurriculum().subscribe((data:any)=>{
      data.forEach(element => {
        let curriculum:CurriculumModel = element.payload.doc.data();
        if(curriculum.uid == uidUser){
          this.curriculumId = element.payload.doc.id;
          this.curriculum = curriculum;
        }
      });

      console.log(this.curriculum);
      this.createForm();
    },(error)=>{
      console.log(error);
    })
  }




}
