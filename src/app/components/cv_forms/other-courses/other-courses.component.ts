import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, FormArray} from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';

@Component({
  selector: 'app-other-courses',
  templateUrl: './other-courses.component.html',
  styleUrls: ['./other-courses.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]
})
export class OtherCoursesComponent implements OnInit {
  myControl = new FormControl();
  hideRequiredControl = new FormControl(false);
  form: FormGroup;
  public idCourse;
  constructor(private _formBuilder: FormBuilder) { }


  ngOnInit() {
    this.createForm();
  }

  send(){

  }

  createForm() {
    this.form = this._formBuilder.group({
      idCourse:[this.idCourse],
      tipoEstudio: ['', Validators.required],
      tituloOtorgado: ['', Validators.required],        
      institucion: ['', Validators.required],
      fechaInicio: ['', Validators.required],
      fechaFinalizacion: ['', Validators.required],
      ciudad: ['', Validators.required],
      descripcion: ['', Validators.required],
    } );
  }
  

}
