import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, FormArray} from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';

@Component({
  selector: 'app-study-form',
  templateUrl: './study-form.component.html',
  styleUrls: ['./study-form.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]
})
export class StudyFormComponent implements OnInit {
  myControl = new FormControl();
  hideRequiredControl = new FormControl(false);
  form: FormGroup;
  public idStudy;
  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createForm(); 
  }

  validateStatus(e){
    if(e.checked){
      this.form.get("fechaFinalizacion").enable();
      this.form.get("fechaFinalizacion").setValidators([Validators.required]);
    }else{
      this.form.get("fechaFinalizacion").disable();
    }
    this.form.get("fechaFinalizacion").updateValueAndValidity();
  }

  send(){
    
  }

  createForm() {
    this.form = this._formBuilder.group({
      idStudy:[this.idStudy],
      nivelEstudios: ['', Validators.required],
      estado: ['', Validators.required],        
      tituloOtorgado: ['', Validators.required],
      fechaInicio: ['', Validators.required],
      fechaFinalizacion: ['', Validators.required],
      nombreInstitucion: ['', Validators.required],
    } );
  }
}
