import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';

@Component({
  selector: 'app-work-experience',
  templateUrl: './work-experience.component.html',
  styleUrls: ['./work-experience.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]
})
export class WorkExperienceComponent implements OnInit {
  myControl = new FormControl();
  hideRequiredControl = new FormControl(false);
  form: FormGroup;
  public idWorkEx;
  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createForm(); 
  }

  send(){
      
  }

  createForm() {
    this.form = this._formBuilder.group({
      idWorkEx:[this.idWorkEx],
      nombreEmpresa: ['', Validators.required],        
      fechaInicio: ['', Validators.required],
      fechaFinalizacion: ['', Validators.required],
      nombreCargo: ['', Validators.required],
      nivelCargo: ['', Validators.required],
      areaDepartamento: ['', Validators.required],
      logrosRespon: ['', Validators.required],
      telefonoEmpre: ['', Validators.required],
      ciudadEmpre: ['', Validators.required],
    } );
  }
}
