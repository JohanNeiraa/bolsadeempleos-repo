import { Component, OnInit } from "@angular/core";
import { EnterpriseModel } from "../../models/enterprise.model";
import { AuthService } from "../../services/auth.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { EnterpriseService } from "../../services/enterprise-service.service";
import swal from "sweetalert2";

@Component({
  selector: "app-edit-enterprise",
  templateUrl: "./edit-enterprise.component.html",
  styleUrls: ["./edit-enterprise.component.css"],
})
export class EditEnterpriseComponent implements OnInit {
  enterprise: EnterpriseModel;
  form: FormGroup;
  uidEnterprise: string;

  constructor(
    private auth: AuthService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private enterpriseService: EnterpriseService
  ) {
    this.createForm();
  }
  ngOnInit() {
    this.uidEnterprise = this.activeRoute.snapshot.params.id;
    this.preloadEnterpaise(this.uidEnterprise);
  }

  preloadEnterpaise(uidEnterpaise: string) {
    let enterprise;
    this.enterpriseService
      .getEnterprise(uidEnterpaise)
      .subscribe((resp: any) => {
        enterprise = resp;
        console.log("soy resp");
        console.log(resp);
        this.updateForm(enterprise);
      });
  }

  updateForm(enterprise) {
    console.log();
      this.form.get("razonSocial").setValue(enterprise.razonSocial),
      this.form.get("nit").setValue(enterprise.nit),
      this.form.get("actividadPrincipal").setValue(enterprise.actividadPrincipal),
      this.form.get("email").setValue(enterprise.email),
      this.form.get("email2").setValue(enterprise.email2),
      this.form.get("telefono").setValue(enterprise.telefono),
      this.form.get("direccion").setValue(enterprise.direccion);
      this.form.get("uid").setValue(this.uidEnterprise);
      
      console.log(enterprise.activa)
      if(enterprise.activa==true){
        console.log('holi')
        this.form.get("activa").setValue(true);
      }else{
        this.form.get("activa").setValue(false);
      }

  }

  updateInfoEnterpraise() {
    console.log("soy el form servicio");

    console.log(this.form.value);
    let data = this.form.value;
    //  console.log(this.form.value);
    this.enterpriseService
      .updateEnterprise(this.uidEnterprise, data)
      .then(() => {
        this.resetform();
      });
  }

  resetform() {
    swal
      .fire({
        title: "Se ah editado correctamente el cliente",
        confirmButtonText: "Aceptar",
      })
      .then(() => {
        this.router.navigateByUrl("admin-profile");
      });
  }

  createForm() {
    this.form = this.fb.group({
      uid: [""],
      razonSocial: ["", Validators.required],
      nit: [""],
      actividadPrincipal: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      email2: ["", Validators.required],
      telefono: ["", Validators.required],
      direccion: ["", Validators.required],
      activa:[''],
    });
  }
}
