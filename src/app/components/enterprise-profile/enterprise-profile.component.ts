import { Component, OnInit } from '@angular/core';
import { EnterpriseService } from '../../services/enterprise-service.service';
import { EnterpriseModel } from '../../models/enterprise.model';

@Component({
  selector: 'app-enterprise-profile',
  templateUrl: './enterprise-profile.component.html',
  styleUrls: ['./enterprise-profile.component.css']
})
export class EnterpriseProfileComponent implements OnInit {

  constructor(private enterpriseService : EnterpriseService) { }
  enterpriseInfo = new EnterpriseModel();

  ngOnInit() {
    this.getEnterpriseInfo();
  }

  getEnterpriseInfo() {

    this.enterpriseService.getEnterpriseInfo(localStorage.getItem("enterpriseuid")).subscribe((resp: any) => {
      this.enterpriseInfo = this.enterpriseService.parseEnterpriseModel(resp);
    });

    
  }

}
