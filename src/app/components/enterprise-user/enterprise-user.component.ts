import { Component, OnInit } from "@angular/core";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { EnterpriseService } from '../../services/enterprise-service.service';

/**
 * @title Table with expandable rows
 */


@Component({
  selector: "app-enterprise-user",
  styleUrls: ["./enterprise-user.component.css"],
  templateUrl: "./enterprise-user.component.html",
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({ height: "0px", minHeight: "0" })),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class EnterpriseUserComponent implements OnInit {

  dataSource = ELEMENT_DATA;
  columnsToDisplay = ["name", "area", "vacante", "HV"];
  expandedElement: PeriodicElement | null;

  public offers = [];
  public validateOffers = [];
  constructor(public enterpriseService: EnterpriseService) {
    this.getOffert();
  }

  //Obtenemos las ofertas existentes de firebase y las guardamos en el arreglo offers
  getOffert() {
    this.enterpriseService.getOffer().subscribe((resp: any) => {
      this.offers = [];
      this.validateOffers = [];
      resp.forEach((offer: any) => {
        console.log(offer.payload.doc.data())
        this.offers.push(offer.payload.doc.data());
      });
      this.validateEnterpriseOffer(this.offers);
    });
  }

  //Validamos que las ofertas pertenezcan a la empresa logueada
  validateEnterpriseOffer(offers) {
    offers.forEach((validOffer) => {
      if (validOffer.idEmpresa == localStorage.getItem("enterpriseuid")) {
        this.validateOffers.push(this.enterpriseService.parseModel(validOffer));
      }
    });

    console.log(this.validateOffers);
  }

  ngOnInit() {}
}

export interface PeriodicElement {
  name: string;
  HV: number;
  area: string;
  vacante: number;
  description: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {
    HV: 4,
    name: "Oferta #1",
    area: "IT",
    vacante: 10,
    description: `Descripción de la oferta, restricciones y demas datos de interes.
    Descripción de la oferta, restricciones y demas datos de interes.
    Descripción de la oferta, restricciones y demas datos de interes.`,
  },
  {
    HV: 3,
    name: "Oferta #2",
    area: "RRHH",
    vacante: 2,
    description: `Descripción de la oferta, restricciones y demas datos de interes.
    Descripción de la oferta, restricciones y demas datos de interes.
    Descripción de la oferta, restricciones y demas datos de interes.`,
  },
  {
    HV: 3,
    name: "Oferta #3",
    area: "Financiera",
    vacante: 6,
    description: `Descripción de la oferta, restricciones y demas datos de interes.
    Descripción de la oferta, restricciones y demas datos de interes.
    Descripción de la oferta, restricciones y demas datos de interes.`,
  },
  {
    HV: 10,
    name: "Oferta #4",
    area: "Comercial",
    vacante: 10,
    description: `Descripción de la oferta, restricciones y demas datos de interes.
    Descripción de la oferta, restricciones y demas datos de interes.
    Descripción de la oferta, restricciones y demas datos de interes.`,
  },
];
