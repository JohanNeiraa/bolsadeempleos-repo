import { AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { EnterpriseModel } from '../../models/enterprise.model';
import { AuthService } from '../../services/auth.service';
import { RouterModule, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { auth } from 'firebase/app';
import { UserModel } from '../../models/user.model';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {

  username: string;
  password: string;
  form: FormGroup;
  formEmpresa: FormGroup;
  enterprise: EnterpriseModel;
  userModel: UserModel;
    
  constructor(private auth:AuthService, private router: Router, private fb: FormBuilder, private fbEmpresa:FormBuilder,private _snackBar: MatSnackBar) { 
    this.createForm();
   /* this.createFormEmpresa();*/
  }

  ngOnInit() {
    this.enterprise = new EnterpriseModel();
    this.userModel = new UserModel();
  }

  createForm(){
    this.form = this.fb.group({
        nombre: ['',Validators.required],
        contraseña: ['',Validators.required]
    })
  }

  loginUser(){
      this.auth.loginUser(this.userModel).subscribe((data:any)=>{
        if(data != null){
          localStorage.setItem('useruid', data.localId);
          this.router.navigate(['/user-profile']);
          console.log(data.localId);
        }
        console.log(data);
      },(error)=>{
        console.log(error);
        this.openSnackBar('Usuario No registrado','');        
      })
  }


  loginEnterprise(){
     console.log(this.enterprise);
       
     this.auth.loginEnterprise(this.enterprise).subscribe((data:any)=>{
        if(data != null){
          //ingresar pagina de empresa
          this.validateLogin(data.localId);
        }
     },(error)=>{
        this.openSnackBar('Empresa no registrada','');
     });
  }

  validateLogin(localUid){
    this.auth.validateLogin(localUid);
  }




  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
