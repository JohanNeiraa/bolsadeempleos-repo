import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferRegistrationComponent } from './offer-registration.component';

describe('OfferRegistrationComponent', () => {
  let component: OfferRegistrationComponent;
  let fixture: ComponentFixture<OfferRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
