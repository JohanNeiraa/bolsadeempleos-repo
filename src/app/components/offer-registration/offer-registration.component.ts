import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, FormArray} from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import Swal from 'sweetalert2'
import { EnterpriseService } from '../../services/enterprise-service.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-offer-registration',
  templateUrl: './offer-registration.component.html',
  styleUrls: ['./offer-registration.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]
})
export class OfferRegistrationComponent implements OnInit {
  myControl = new FormControl();
  options: string[] = [ 'SMLV',
                        '1.000.000 a 2.000.000', 
                        '2.000.000 a 3.000.000', 
                        '3.000.000 a 5.000.000',
                        '5.000.000 a 8.000.000'];


  form: FormGroup;
  public idEmpressa;

  hideRequiredControl = new FormControl(false);
  disableSelect = new FormControl(false);

  constructor(private _formBuilder: FormBuilder,
              private enterpriseService: EnterpriseService,
              private _snackBar: MatSnackBar) {}

  ngOnInit() {
    this.idEmpressa = localStorage.getItem('enterpriseuid');
    this.createForm();
  }


  validateLicense(e){
      if(e.checked){
        this.form.get("licencia").enable();
        this.form.get("licencia").setValidators([Validators.required]);
      }else{
        this.form.get("licencia").disable();

      }
      this.form.get("licencia").updateValueAndValidity();


  }

  onCheckboxChange(e) {
    const requisitosOferta: FormArray = this.form.get('requisitosOferta') as FormArray;
    if (e.checked) {
      console.log(e.source.value)
      requisitosOferta.push(new FormControl(e.source.value));
    } else {
      let i: number = 0;
      requisitosOferta.controls.forEach((item: FormControl) => {
        if (item.value == e.souerce.value) {
          requisitosOferta.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  createForm() {
      this.form = this._formBuilder.group({
        idEmpresa:[this.idEmpressa],
        nombre: ['', Validators.required],        
        salario: ['', Validators.required],
        tipoContrato: ['', Validators.required],
        ciudad: ['', Validators.required],
        vacantes: ['', Validators.required],
        infoEmpresa: ['', Validators.required],
        desLabores: ['', Validators.required],
        desConocimientos: ['', Validators.required],
        desCompetencias: ['', Validators.required],
        nivelEstudios: ['', Validators.required],
        experiencia: ['', Validators.required],
        fechaCierre: ['', Validators.required],
        requisitosOferta: this._formBuilder.array([]),
        area: ['', Validators.required],
        licencia: [{value:'', disabled:true}]
      } );
  }

  send() {
    console.log("entre")
    console.log(this.form.value);
    if (this.form.valid) {
      this.createOffer();
    }else{
      this.openSnackBar('Formulario invalido verifique sus campos por favor','');
      //Swal.fire('Formulario invalido verifique sus campos por favor')
    }
  }

  createOffer(){
    const offer = this.form.value;
    this.enterpriseService.newOffer(offer).then(data=>{
      //Swal.fire('Oferta creada correctamente')
      //console.log(data);
      this.openSnackBar('Oferta creada correctamente','');
    })
  }


  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}