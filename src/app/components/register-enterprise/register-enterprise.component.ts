import { Component, OnInit, createPlatformFactory } from "@angular/core";
import { EnterpriseModel } from "../../models/enterprise.model";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { auth } from 'firebase/app';

@Component({
  selector: "app-register-enterprise",
  templateUrl: "./register-enterprise.component.html",
  styleUrls: ["./register-enterprise.component.css"],
})
export class RegisterEnterpriseComponent implements OnInit {
  enterprise: EnterpriseModel;
  form: FormGroup;

  constructor(private auth: AuthService, private router: Router, private fb: FormBuilder) {
  }

  ngOnInit() {

    this.enterprise = new EnterpriseModel();
    this.enterprise.email = "test@test.com";

  }

  validar() { }

  saveNewEnterprise() {
    console.log(this.enterprise);
    this.auth.newEnterpriseUser(this.enterprise).subscribe(
      (data: any) => {
        if (data != null) {
          console.log('este es ', data);
          this.enterprise.uid = data.localId;
          this.enterprise.activa = false;
          this.auth.newEnterpriceData(this.enterprise).then((data) => {
            if (data) {
              localStorage.setItem('enterpriseuid', this.enterprise.uid);
              this.router.navigate(["/enterprise-profile"]);
            }
          });
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
