import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from '../../services/user.service';
import { CurriculumModel } from '../../models/curriculum.model';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  user: UserModel;
  form: FormGroup;

  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router, private userService:UserService) {
  }

  ngOnInit() {
    this.user = new UserModel();
  }

  validar() {}

  saveNewUser() {
    console.log('USER:',this.user);
    this.auth.newUser(this.user).subscribe(
      (data: any) => {
        if (data != null) {
          console.log('este es ', data);
          this.user.uid = data.localId;
          this.auth.newUserData(this.user).then((data) => {
            if (data) {
              localStorage.setItem('useruid', this.user.uid);
              this.saveEmptyCurriculum(this.user);
              this.router.navigate(["/user-profile"]);
            }
          });
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  saveEmptyCurriculum(user){
    this.userService.newCurruculum(user).then((data)=>{
      console.log('curriculum registrado');
    },(error)=>{
        console.log(error);
    })
  }
}
