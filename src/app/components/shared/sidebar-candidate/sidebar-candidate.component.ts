import { Component, OnInit } from '@angular/core';

export interface Section {
  name: string;
  link: string;
}

@Component({
  selector: 'app-sidebar-candidate',
  templateUrl: './sidebar-candidate.component.html',
  styleUrls: ['./sidebar-candidate.component.css']
})


export class SidebarCandidateComponent implements OnInit {

  folders: Section[] = [
    {
      name: 'Aplicaciones',
      link:'/user-profile' 
    },
    {
      name: 'Hoja de vida',
      link:'/candidate-cv'
    },
    {
      name: 'Progreso',
      link:'/user-profile'
    }
  ];

  constructor() { }

  

  ngOnInit() {
  }

}
