import { Component, OnInit } from '@angular/core';

export interface Section {
  name: string;
  link: string;
}

@Component({
  selector: 'app-sidebar-enterprise',
  templateUrl: './sidebar-enterprise.component.html',
  styleUrls: ['./sidebar-enterprise.component.css']
})
export class SidebarEnterpriseComponent implements OnInit {
  
  enterpriseOptions:Section[]=[
    {
      name: 'Ver Perfil',
      link:'/enterprise-profile-info'
    },
  ]

  enterpriseOffers:Section[]=[
    {
      name:'Ofertas',
      link:'/enterprise-profile'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
