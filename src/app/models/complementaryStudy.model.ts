export class ComplementaryStudyModel{
    city: string;
    description: string;
    finalDate: string;
    initDate: string;
    institution: string;
    studyType: string;
    studyTitle: string;


}