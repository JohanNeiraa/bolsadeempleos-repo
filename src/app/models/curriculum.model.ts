import { ExperienceModel } from './experience.model';
import { StudiesModel } from './studies.model';
import { LanguageModel } from './lenguages.model';
import { ComplementaryStudyModel } from './complementaryStudy.model';

export class CurriculumModel{
    uid:string;
    image:string;
    names: string;
    lastName: string;
    secondLastName: string;
    identification: string;
    identificationType: string;
    liveCity:string;
    bornCity: string;
    bornDate: string;
    genre: string;
    civilState: string;
    address: string;
    principalPhone: string;
    secondaryPhone: string;
    email: string;
    profession: string;
    salary:string;
    movility: boolean;
    laboralProfile: string;
    experience: ExperienceModel[];
    principalCarrer: string;
    academicFormation:StudiesModel;
    studies: ComplementaryStudyModel[];
    nativeLanguage : string;
    languages: LanguageModel[];
    habilities: string;
    driverLicence: boolean;
    driverLicenceType: string;
}