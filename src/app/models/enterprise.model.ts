
export class EnterpriseModel{
    
    uid:string;
    razonSocial:string;
    nit:string;
    actividadPrincipal:string;
    email:string;
    email2:string;
    password:string;
    password2: string
    telefono:string;
    direccion:string;
    activa:boolean;
    
}