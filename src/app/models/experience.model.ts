export class ExperienceModel {
    enterpriseName: string;
    initialDate: string;
    finishDate: string;
    jobName: string;
    jobLevel:string;
    area: string;
    achievements:string;
    enterprisePhone:string;
    enterpriseCity:string;
}