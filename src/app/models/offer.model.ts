export class OfferModel{
    
    ciudad:string;
    desCompetencias:string;
    desConocimientos:string;
    desLabores:string;
    experiencia:string;
    fechaCierre:string;
    idEmpresa:string;
    infoEmpresa:string;
    nivelStudios:string;
    nombre:string;
    requisitosOferta:string[];
    salario:string;
    licencia:string;
    tipoContrato:string;
    area:string;
    nivelEstudios:string;
    vacantes:string;
}