export class StudiesModel{
    studyLevel: string;
    studyState: string;
    studyTitle: string;
    initDate: string;
    finalDate: string;
    schoolName: string;
}