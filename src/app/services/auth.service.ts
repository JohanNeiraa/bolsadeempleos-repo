import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnterpriseModel } from '../models/enterprise.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserModel } from '../models/user.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  private url = 'https://identitytoolkit.googleapis.com/v1/accounts:';
  private apiKey = 'AIzaSyB-0qo-Bu4tBk-06iH5obO5EQWvqywQcVI';
  //crear nuevo usuario
  //https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]
  

  //logear nuevo usuario
  // https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]



  constructor(private http: HttpClient, private firestore:AngularFirestore, private router:Router) { }

  logout(){

  }


  loginAdmin(email,password){
    var url = this.url+'signInWithPassword?key='+this.apiKey;

    const authData ={
       email: email,
       password: password,
       returnSecureToken: true
    }

    return this.http.post(url,authData);
  }

  loginEnterprise(enterprise:EnterpriseModel){
    var url = this.url+'signInWithPassword?key='+this.apiKey;

    const authData ={
       email: enterprise.email,
       password: enterprise.password,
       returnSecureToken: true
    }
    return this.http.post(url,authData);
  }

  loginUser(user:UserModel){
    var url = this.url+'signInWithPassword?key='+this.apiKey;

    const authData ={
       email: user.email,
       password: user.password,
       returnSecureToken: true
    }
    return this.http.post(url,authData);
  }

  newEnterpriseUser(enterprise:EnterpriseModel){
    
    var url = this.url+'signUp?key='+this.apiKey;

    const authData ={
       email: enterprise.email,
       password: enterprise.password,
       returnSecureToken: true
    }
    return this.http.post(url,authData);
  }

  newUser(user:UserModel){
    //console.log('llega');
    var url = this.url+'signUp?key='+this.apiKey;

    const authData ={
       email: user.email,
       password: user.password,
       returnSecureToken: true
    }
    console.log('llega');
    return this.http.post(url,authData);
  }

  newEnterpriceData(enterprise:EnterpriseModel){
    //Almacenar datos en firebase 
    return new Promise<any>((resolve, reject) =>{
      this.firestore
          .collection("enterprise").doc(enterprise.uid)
          .set(JSON.parse(JSON.stringify(enterprise)))
          .then(res => {
            resolve(true);
          }, err => 
            reject(err)
          );
  });
  }

  newUserData(user:UserModel){
    //Almacenar datos en firebase 
    return new Promise<any>((resolve, reject) =>{
      this.firestore
          .collection("user").doc(user.uid)
          .set(JSON.parse(JSON.stringify(user)))
          .then(res => {
            resolve(true);
          }, err => 
            reject(err)
          );
    });
  }


  validateLogin(uid){
    console.log(uid);
    var isEnterprise = this.firestore.collection("enterprise").doc(uid);
    
    isEnterprise.get().subscribe((data:any)=>{
        if(data.exists){
          localStorage.setItem('enterpriseuid', data.localId);
          this.router.navigate(['/enterprise-profile']);
          console.log(data.localId);
        }else{
          var isAdmin = this.firestore.collection('admin').doc(uid);
          isAdmin.get().subscribe((admin:any)=>{
              if(admin.exists){
                localStorage.setItem('adminuid', admin.localId);
                this.router.navigate(['/admin-profile']);
                console.log(data.localId);
              }
          });
        }
    });
  }
}
