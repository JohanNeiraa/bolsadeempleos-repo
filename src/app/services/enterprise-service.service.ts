import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { OfferModel } from '../models/offer.model';
import { EnterpriseModel } from '../models/enterprise.model';

@Injectable({
  providedIn: 'root'
})
export class EnterpriseService {

  constructor(private firestore: AngularFirestore) { }
  
  getOffer(){
    return this.firestore.collection('offers').snapshotChanges();
  }

  newOffer(offer: any){
    return new Promise<any>((resolve, reject) =>{  
      this.firestore
          .collection("offers").add(offer)
          .then(res => {
            resolve(true);
          }, err => 
            reject(err)
          );
  });    
  }

  getEnterpriseList(){
    return this.firestore.collection('enterprise').snapshotChanges();
  }

  getEnterprise(uid: string){
    console.log(uid);
    return this.firestore.collection('enterprise').doc(uid).valueChanges();
  }

  updateEnterprise(uid: string, data: any){
    console.log(uid);
    return this.firestore.collection('enterprise').doc(uid).set(data);
  }

  deleteEnterprice(uidDelete){
    this.firestore.collection('enterprise').doc(uidDelete).delete().then(function() {
        console.log("Document successfully deleted!");
    }).catch(function(error) {
        console.error("Error removing document: ", error);
    });
  }

  getEnterpriseInfo(uidEnterprise){
    return this.firestore.collection('enterprise').doc(uidEnterprise).valueChanges();
  }

  parseModel(offer:any ){
    let offerModel = new OfferModel();
    offerModel.ciudad = offer.ciudad;
    offerModel.desCompetencias = offer.desCompetencias;
    offerModel.desConocimientos = offer.desConocimientos;
    offerModel.desLabores = offer.desLabores;
    offerModel.experiencia = offer.experiencia;
    offerModel.fechaCierre = offer.fechaCierre;
    offerModel.idEmpresa = offer.idEmpresa;
    offerModel.infoEmpresa = offer.infoEmpresa;
    offerModel.licencia = offer.desCompetencias;
    offerModel.nivelEstudios = offer.nivelEstudios;
    offerModel.nombre = offer.nombre;
    offerModel.requisitosOferta = offer.requisitosOferta;
    offerModel.salario = offer.salario;
    offerModel.tipoContrato = offer.tipoContrato;
    offerModel.vacantes = offer.tipoContrato;
    offerModel.area = offer.area;
    return offerModel;
  }

  parseEnterpriseModel(enterprice:any){
    let enterpriseModel = new EnterpriseModel();
    enterpriseModel.uid = enterprice.uid;
    enterpriseModel.actividadPrincipal = enterprice.actividadPrincipal;
    enterpriseModel.direccion = enterprice.direccion;
    enterpriseModel.email = enterprice.email;
    enterpriseModel.nit = enterprice.nit;
    enterpriseModel.razonSocial = enterprice.razonSocial;
    enterpriseModel.telefono = enterprice.telefono;
    enterpriseModel.activa = enterprice.activa;
    return enterpriseModel;
  }
}


