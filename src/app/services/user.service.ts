import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { CurriculumModel } from '../models/curriculum.model';
import { ExperienceModel } from '../models/experience.model';
import { LanguageModel } from '../models/lenguages.model';
import { StudiesModel } from '../models/studies.model';
import { ComplementaryStudyModel } from '../models/complementaryStudy.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private firestore: AngularFirestore) { }

  newCurruculum(user){
    var curriculum = this.emptyCurriculum(user);
    return new Promise<any>((resolve, reject) =>{  
      this.firestore
          .collection("curriculum").add({...curriculum})
          .then(res => {
            resolve(true);
          }, err => 
            reject(err)
          );
    });    
  }

  getUserCurriculum(){
    return this.firestore.collection('curriculum').snapshotChanges();
  }

  updateCurriculum(curriculumForm,idcurriculum,idusuario,currentCurriculum){
    console.log(curriculumForm);
    let curriculum = this.parseModel(curriculumForm,idusuario,currentCurriculum);
    console.log(curriculum);
    return this.firestore.collection('curriculum').doc(idcurriculum).set(curriculum);
  }

  updateLanguages(languageForm,curriculumUser,idcurriculum){
    let lanModel :LanguageModel ={
      idiom :languageForm.idiomaaaa,
      level: languageForm.nivelIdioma
    };

    let curriculumModel:CurriculumModel = curriculumUser;
    curriculumModel.languages.push(lanModel);
    return this.firestore.collection('curriculum').doc(idcurriculum).set(curriculumModel);
  }

  updateComplementaryStudies(complementaryForm,curriculumUser,idcurriculum){
    let stuModel: ComplementaryStudyModel={
      finalDate : complementaryForm.fechaFinalizacion,
      initDate:complementaryForm.fechaInicio,
      institution: complementaryForm.institucion,
      studyTitle:complementaryForm.tituloOtorgado,
      city: complementaryForm.ciudad,
      description: complementaryForm.descripcion,
      studyType: complementaryForm.tipoEstudio
    };

    console.log(complementaryForm);
    console.log(stuModel);
    
    let curriculumModel:CurriculumModel = curriculumUser;
    curriculumModel.studies.push(stuModel);
    return this.firestore.collection('curriculum').doc(idcurriculum).set(curriculumModel);
  }

  deleteExperience(curriculumUser,idcurriculum,index){
    let curriculumModel:CurriculumModel = curriculumUser;
    if (index !== -1) {
      curriculumModel.experience.splice(index, 1);
    }
    return this.firestore.collection('curriculum').doc(idcurriculum).set(curriculumModel);
  }

  deleteComplementary(curriculumUser,idcurriculum,index){
    let curriculumModel:CurriculumModel = curriculumUser;
    if (index !== -1) {
      curriculumModel.studies.splice(index, 1);
    }
    return this.firestore.collection('curriculum').doc(idcurriculum).set(curriculumModel);
  }

  deleteLanguage(curriculumUser,idcurriculum,index){
    let curriculumModel:CurriculumModel = curriculumUser;
    if (index !== -1) {
      curriculumModel.languages.splice(index, 1);
    }
    return this.firestore.collection('curriculum').doc(idcurriculum).set(curriculumModel);
  }


  updateLaboralExperience(experienceForm,currentUserModel,idcurriculum){
    
    let expModel :ExperienceModel = {
      achievements: experienceForm.logrosRespon, 
      area:experienceForm.areaDepartamento, 
      enterpriseCity: experienceForm.ciudadEmpre,
      enterpriseName: experienceForm.nombreEmpresa,
      enterprisePhone: experienceForm.telefonoEmpre,
      finishDate: experienceForm.fechaFinalizacion,
      initialDate: experienceForm.fechaInicio,
      jobLevel: experienceForm.nivelCargo,
      jobName:experienceForm.nombreCargo
    };
    
    console.log(expModel);
    let curriculumModel:CurriculumModel = currentUserModel;
    curriculumModel.experience.push(expModel);
    return this.firestore.collection('curriculum').doc(idcurriculum).set(curriculumModel);
  }
  
  parseModel(curriculumForm,idusuario,currentCurriculum){
    let expModel :ExperienceModel = {
      achievements:'', 
      area:'', 
      enterpriseCity:'',
      enterpriseName:'',
      enterprisePhone:'',
      finishDate:'',
      initialDate:'',
      jobLevel:'',
      jobName:''
    };
    
    let lanModel :LanguageModel ={
      idiom :'',
      level:''
    };

    let stuModel: StudiesModel={
      finalDate : curriculumForm.formacionAcademica.fechaFinalizacion,
      initDate:curriculumForm.formacionAcademica.fechaInicio,
      schoolName: curriculumForm.formacionAcademica.nombreInstitucion,
      studyLevel: curriculumForm.formacionAcademica.nivelEstudios,
      studyState:curriculumForm.formacionAcademica.estado,
      studyTitle:curriculumForm.formacionAcademica.tituloOtorgado
    };

    var experience:ExperienceModel[] = [expModel];
    var languages: LanguageModel[] = [lanModel];


    let usercurr: CurriculumModel={
      uid : idusuario,
      address : curriculumForm.DireccionResidencia,
      names : curriculumForm.nombres,
      lastName : curriculumForm.apellido1,
      email : curriculumForm.correoElectronico,
      principalPhone : curriculumForm.telefonoPrincipal,
      bornCity : curriculumForm.ciudadNacimiento,
      bornDate : curriculumForm.fechaNacimiento,
      civilState : curriculumForm.estadoCivil,
      driverLicence : false,
      image : null,
      driverLicenceType: curriculumForm.licencia,
      experience: currentCurriculum.experience,
      genre: curriculumForm.genero,
      habilities:curriculumForm.destrezas,
      identification :curriculumForm.numeroIdentificación,
      identificationType:curriculumForm.tipoIdentificacion,
      laboralProfile:curriculumForm.perfilLaboral,
      languages: currentCurriculum.languages,
      movility: false,
      academicFormation: stuModel,
      principalCarrer :curriculumForm.carreraPrincipal,
      profession: curriculumForm.profesion,
      salary:curriculumForm.aspiracionSalarial,
      secondLastName:curriculumForm.apellido2,
      secondaryPhone:curriculumForm.telefonoSecundario,
      nativeLanguage: curriculumForm.lenguaNativa,
      liveCity: curriculumForm.CiudadResidencia,
      studies:currentCurriculum.studies,
    }

    return usercurr;
  }
  
  emptyCurriculum(user){
    let expModel :ExperienceModel = {
      achievements:'', 
      area:'', 
      enterpriseCity:'',
      enterpriseName:'',
      enterprisePhone:'',
      finishDate:'',
      initialDate:'',
      jobLevel:'',
      jobName:''
    };
    
    let lanModel :LanguageModel ={
      idiom :'',
      level:''
    };

   
    let stuModel: StudiesModel={
      finalDate :'',
      initDate:'',
      schoolName:'',
      studyLevel:'',
      studyState:'',
      studyTitle:''
    };

    var experience:ExperienceModel[] = [];
    var languages: LanguageModel[] = [];
    var studies: StudiesModel[] =[];
    var complementaries: ComplementaryStudyModel[]=[];


    let usercurr: CurriculumModel={
      uid : user.uid,
      image:'',
      address : user.direccion,
      names : user.nombre,
      lastName : user.apellido,
      email : user.email,
      principalPhone : user.telefono,
      bornCity : '',
      bornDate : '',
      civilState : '',
      driverLicence : false,
      driverLicenceType:'',
      experience:experience,
      genre:'',
      habilities:'',
      identification :'',
      identificationType:'',
      laboralProfile:'',
      languages: languages,
      movility: false,
      principalCarrer :'',
      profession: '',
      academicFormation: stuModel,
      salary:'',
      secondLastName:'',
      secondaryPhone:'',
      studies:complementaries,
      liveCity:'',
      nativeLanguage:''
    }

    return usercurr;
  }
}
