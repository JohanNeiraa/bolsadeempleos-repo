// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB-0qo-Bu4tBk-06iH5obO5EQWvqywQcVI",
    authDomain: "bolsaempleo-4b5c0.firebaseapp.com",
    databaseURL: "https://bolsaempleo-4b5c0.firebaseio.com",
    projectId: "bolsaempleo-4b5c0",
    storageBucket: "bolsaempleo-4b5c0.appspot.com",
    messagingSenderId: "214263212017",
    appId: "1:214263212017:web:3a7887ae455ddfc488c650"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
